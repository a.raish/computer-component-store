function sendRequest(method, query, fun) {
        var xhr = new XMLHttpRequest()
        xhr.open(method, query, true)
        xhr.onload = fun
        xhr.send()
        return xhr
    }

function addCategories(cats) {
    var i, parId, par, p = document.getElementById('categories');
    for (i in cats) {
        var n = cats[i]["id"].split(".").length - 1
        cats[i]["id"] = cats[i]["id"].split(".").join("a");

        var li = document.createElement("li")
        var a = document.createElement("a")
        var ul = document.createElement("ul")
        li.className = "active"
        a.setAttribute("href", "#cat" + cats[i]["id"])
        a.setAttribute("data-toggle", "collapse")
        a.setAttribute("aria-expanded", "false")
        a.className = "dropdown-toggle"
        a.text = cats[i]["id"].split("a").join(".") + ". " + cats[i]["name"]
        a.style = "font-size: " + (1 - 0.1 * n) + "em;"
        ul.className = "collapse list-unstyled"
        ul.id = "cat" + cats[i]["id"]
        ul.style = "padding-left: " + 30 * n + "px !important;"
        li.appendChild(a)
        li.appendChild(ul)

        if (!cats[i]["id"].includes('a')) {
            p.appendChild(li)
        } else {
            parId = "cat" + cats[i]["id"].substring(0, cats[i]["id"].lastIndexOf("a"))
            par = document.getElementById(parId);
            par.appendChild(li)
        }
    }
}

function createItemsTable(items, type) {
    clearField()
    var itemsInRow = 0;
    var f = document.getElementById('field')
    if (items.length === 0) {
        var d = document.createElement("div")
        d.innerHTML = "No items in this category for such filters"
        d.style = "font-size: 20px;"
        f.appendChild(d)
        return
    }
    var i, id, text, img, r = document.createElement("div")
    r.style = "margin-bottom: 15px;"
    r.className = "row"
    for (i in items) {
        if (itemsInRow === rowLen) {
            f.appendChild(r)
            r = document.createElement("div")
            r.style = "margin-bottom: 15px;"
            r.className = "row"
            itemsInRow = 0
        }
        if (type === "product") {
            id = "p" + items[i]["id"]
            text = "Name: " + items[i]["name"] + "\nPrice: " + items[i]["price"] + "\nWarranty_period: " + items[i]["warranty_period"]
            img = items[i]["img_url"]
            addItem(r, id, img, text)
        } else if (type === "brand") {
            id = "b" + items[i]["id"]
            text = "Name: " + items[i]["name"] + "\nDescription: " + items[i]["description"]
            img = items[i]["img_url"]
            addItem(r, id, img, text)
        }
        itemsInRow += 1
    }
    if (itemsInRow !== 0) {
        f.appendChild(r)
    }
}

function addItem(parent, id, img, text) {
    var col = document.createElement("div")
    var image = document.createElement("img")
    col.className = "col"
    col.style = "max-width: 200px; max-height: 300px; margin-right: 15px; float: left; "
    col.id = id
    image.className = "card-img-top"
    image.style = "width: 200px; height: 200px;"
    image.setAttribute("src", img)
    var ta = document.createElement("textarea")
    ta.className = "shop-text"
    ta.style = "min-width: 200px; min-height: 100px; max-width: 200px; max-height: 100px; background-color: #99c2ff"
    ta.setAttribute("readonly", "readonly")
    ta.value = text
    col.appendChild(image)
    col.appendChild(ta)
    parent.appendChild(col)
}

function clearField() {
    var f = document.getElementById("field")
    for (var i = f.childNodes.length - 1; i > -1; i--) {
        f.childNodes[i].remove()
    }
}

function fillSelect(name, values) {
    var select, v
    select = document.getElementById(name)
    for (var i = select.childNodes.length - 1; i > -1; i--) {
        select.childNodes[i].remove()
    }
    for (v in values) {
        var opt = document.createElement("option")
        opt.value = values[v]["id"]
        opt.text = values[v]["name"]
        select.appendChild(opt)
    }
    $('#' + name).trigger('change')
}

function getById(from, id) {
    var res
    for (v in from) {
        if (from[v]["id"] === id) {
            res = from[v]
            break
        }
    }
    return res
}

function openTab(evt, tabName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent")
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none"
    }
    tablinks = document.getElementsByClassName("tablinks")
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "")
    }
    document.getElementById(tabName).style.display = "block"
    evt.currentTarget.className += " active"
}
