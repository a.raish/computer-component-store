from flask import Flask, make_response, render_template, request, send_from_directory
from flask_httpauth import HTTPBasicAuth
import psycopg2
import json
import os

from flask import g
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for
import functools

DB = "postgres"
HOST = "postgres"
USER = "postgres"
PWD = "postgres"

ADMIN_LOGIN = "admin"
ADMIN_PASSWORD = "admin"
SECRET_KEY = "f3cfe9ed8fae309f02079dbf"

app = Flask(__name__)
app.config['SECRET_KEY'] = SECRET_KEY
auth = HTTPBasicAuth()
conn = psycopg2.connect(dbname=DB, user=USER, password=PWD, host=HOST, connect_timeout=100)


# It here to fix cycle importing
from shop.service import *


@app.route("/", methods=["GET"])
def main():
    shop_info = get_shop_info()
    categories = get_categories()
    app.logger.info("Get main page")
    return render_template('MainPage.html', categories=json.dumps(categories), shop_info=json.dumps(shop_info))


@app.route('/products', methods=["GET"])
def products():
    category = request.args.get('category')
    products = get_products(category)
    app.logger.info("Get products for category: %s", category)
    return make_response(json.dumps({"action": "setProducts", "products": products}), 200,
                         {"content_type": "application/json"})


@app.route('/filter_values', methods=["GET"])
def filter_values():
    category = request.args.get('category')
    values = get_filter_values(category)
    app.logger.info("Get filter values for category: %s", category)
    return make_response(json.dumps({"action": "setFilterValues", "values": values}), 200,
                         {"content_type": "application/json"})


@app.route('/filtered_products', methods=["GET"])
def filtered_products():
    params = {
        "category": request.args.get('category'),
        "min_price": request.args.get('min_price'),
        "max_price": request.args.get('max_price'),
        "min_warranty": request.args.get('min_warranty'),
        "max_warranty": request.args.get('max_warranty'),
        "installment": request.args.get('installment')
    }
    products = get_filtered_products(params)
    app.logger.info("Get filtered products for category: %s", category)
    return make_response(json.dumps({"action": "setFilteredProducts", "products": products}), 200,
                         {"content_type": "application/json"})


@app.route('/low_priced_products', methods=["GET"])
def low_priced_products():
    params = {
        "category": request.args.get('category'),
        "n": request.args.get('n')
    }
    products = get_low_priced_products(params)
    app.logger.info("Get low priced products for category: %s", category)
    return make_response(json.dumps({"action": "setLowPricedProducts", "products": products}), 200,
                     {"content_type": "application/json"})


@app.route('/high_priced_products', methods=["GET"])
def high_priced_products():
    params = {
        "category": request.args.get('category'),
        "n": request.args.get('n')
    }
    products = get_high_priced_products(params)
    app.logger.info("Get high priced products for category: %s", category)
    return make_response(json.dumps({"action": "setHighPricedProducts", "products": products}), 200,
                     {"content_type": "application/json"})


@app.route('/category_path', methods=["GET"])
def category_path():
    category = request.args.get('category')
    path = get_category_path(category)
    app.logger.info("Get path to category: %s", category)
    return make_response(json.dumps({"action": "setPath", "path": path}), 200,
                         {"content_type": "application/json"})


@app.route('/category_info', methods=["GET"])
def category_info():
    category = request.args.get('category')
    info = get_category_info(category)
    app.logger.info("Get info for category: %s", category)
    return make_response(json.dumps({"action": "setCategoryInfo", "info": info}), 200,
                         {"content_type": "application/json"})


@app.route('/product_info', methods=["GET"])
def product_info():
    id = request.args.get('id')
    products = get_product_info(id)
    app.logger.info("Get info for product: %s", id)
    return make_response(json.dumps({"action": "setProductInfo", "products": products}), 200,
                        {"content_type": "application/json"})


def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if session.get("user") is None:
            return redirect(url_for("login"))
        return view(**kwargs)

    return wrapped_view


@app.route("/login", methods=("GET", "POST"))
def login():
    if request.method == "POST":
        username = request.form["username"]
        password = request.form["password"]

        if username == ADMIN_LOGIN and password == ADMIN_PASSWORD:
            session.clear()
            session["user"] = username
            return redirect(url_for("admin"))
    return render_template("Login.html")


@app.route('/admin', methods=["GET"])
@login_required
def admin():
        shop_info = get_shop_info()
        categories = get_categories()
        node_categories = get_node_categories()
        leaf_categories = get_leaf_categories()
        products = get_all_products()
        app.logger.info("Get admin page")
        return render_template('AdminPage.html',
                               categories=json.dumps(categories),
                               node_categories=json.dumps(node_categories),
                               leaf_categories=json.dumps(leaf_categories),
                               products=json.dumps(products),
                               shop_info=json.dumps(shop_info))


@app.route('/category', methods=["POST", "DELETE"])
@login_required
def category():
    if request.method == 'POST':
        params = {
            "id": request.args.get('id'),
            "parent": request.args.get('parent'),
            "name": request.args.get('name')
        }
        add_or_update_category(params)
        app.logger.info("Add or update category: %s, %s", params["id"], params["name"])
    else:
        id = request.args.get('id')
        app.logger.info("Remove category: %s", id)
        remove_category(id)
    categories = get_categories()
    node_categories = get_node_categories()
    leaf_categories = get_leaf_categories()
    return make_response(json.dumps({"action": "setCategories", "node_categories": node_categories,
                                     "leaf_categories": leaf_categories, "categories": categories}), 200,
                         {"content_type": "application/json"})


@app.route('/product', methods=["POST", "DELETE"])
@login_required
def product():
    if request.method == "POST":
        params = {
            "id": request.args.get('id'),
            "name": request.args.get('name'),
            "price": request.args.get('price'),
            "plan": request.args.get('plan'),
            "period": request.args.get('period'),
            "num": request.args.get('num'),
            "image": request.args.get('image'),
            "desc": request.args.get('desc'),
        }
        app.logger.info("Add or update product: %s, %s", params["id"], params["name"])
        add_or_update_product(params)
    else:
        id = request.args.get('id')
        app.logger.info("Remove product: %s", id)
        remove_product(id)
    products = get_all_products()
    return make_response(json.dumps({"action": "setProducts", "products": products}), 200,
                         {"content_type": "application/json"})


@app.route('/shop', methods=["POST"])
@login_required
def shop():
    params = {
        "name": request.args.get('name'),
        "phone": request.args.get('phone'),
        "address": request.args.get('address'),
    }
    update_shop_info(params)
    shop_info = get_shop_info()
    app.logger.info("Update shop info: %s, %s, %s", params["name"], params["phone"], params["address"])
    return make_response(json.dumps({"action": "setShopInfo", "shop_info": shop_info}), 200,
                         {"content_type": "application/json"})


@app.route('/logout')
def logout():
    session.clear()
    return redirect(url_for("main"))


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static/images'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')
