from shop import conn


def get_categories():
    cursor = conn.cursor()
    cursor.execute("""select * from category""")
    records = cursor.fetchall()
    res = []
    for r in sorted(records, key=lambda x: len(x[0])):
        res.append({"id": r[0], "name": r[1]})
    return res


def get_node_categories():
    cursor = conn.cursor()
    cursor.execute("""select category.cat_num cat_num, category.cat_name from category where cat_num not in 
                  (select category.cat_num from product, category  where product.cat_num = category.cat_num)""")
    records = cursor.fetchall()
    res = []
    for r in sorted(records, key=lambda x: len(x[0])):
        res.append({"id": r[0], "name": r[1]})
    return res


def get_leaf_categories():
    cursor = conn.cursor()
    cursor.execute("""select cat_num, cat_name from category where cat_num not in (select distinct f.cat_num from 
                        category f, category f1 where f1.cat_num like f.cat_num || '.' || '%');""")
    records = cursor.fetchall()
    res = []
    for r in sorted(records, key=lambda x: len(x[0])):
        res.append({"id": r[0], "name": r[1]})
    return res


def get_category_path(cat_num):
    cursor = conn.cursor()
    res = ""
    while cat_num != "":
        cursor.execute("""select cat_name from category where cat_num = %(cat_num)s""", {"cat_num": cat_num})
        records = cursor.fetchall()
        res = records[0][0] + "/" + res
        cat_num = cat_num[:cat_num.rfind(".")]
    return res[:-1]


def get_category_info(cat_num):
    cursor = conn.cursor()
    cursor.execute("""select avg(price), min(price), max(price), count(price) from product where cat_num = %(cat_num)s""", {"cat_num": cat_num})
    records = cursor.fetchall()
    res = {}
    if records[0][0]:
        res["avg_price"] = round(float(records[0][0]), 2)
    else:
        res["avg_price"] = 0
    if records[0][1]:
        res["min_price"] = round(float(records[0][1]), 2)
    else:
        res["min_price"] = 0
    if records[0][2]:
        res["max_price"] = round(float(records[0][2]), 2)
    else:
        res["max_price"] = 0
    if records[0][3]:
        res["count"] = int(records[0][3])
    else:
        res["count"] = 0

    return res


def add_or_update_category(data):
    cursor = conn.cursor()
    if data["id"] == "-1":
        cursor.callproc('add_category', [len(data["parent"].split(".")) + 1, data["name"], data["parent"], ])
    else:
        cursor.execute("""update category set cat_name = %(cat_name)s where cat_num = %(cat_num)s""",
                       {"cat_name": data["name"], "cat_num": data["id"]})
    conn.commit()


def remove_category(id):
    cursor = conn.cursor()
    cursor.execute("""delete from category where cat_num = %(cat_num)s;""", {"cat_num": id})
    cursor.execute("""delete from product where cat_num = %(cat_num)s;""", {"cat_num": id})
    conn.commit()


def get_all_products():
    cursor = conn.cursor()
    cursor.execute("""select p.prod_id, p.prod_name, p.price, p.installment_plan, p.warranty_period, p.img_url, 
        p.description, p.cat_num from product p;""")
    records = cursor.fetchall()
    res = []
    for r in records:
        res.append({"id": r[0], "name": r[1], "price": r[2], "installment_plan": r[3], "warranty_period": r[4],
                    "img_url": r[5], "description": r[6], "cat_num": r[7]})
    return res


def get_products(cat_num):
    cursor = conn.cursor()
    cursor.execute("""select * from product where cat_num = %(cat_num)s order by price desc""", {"cat_num": cat_num})
    records = cursor.fetchall()
    res = []
    for r in records:
        res.append({"id": r[0], "name": r[1], "price": r[2], "installment_plan": r[3],
                    "warranty_period": r[4], "img_url": r[5], "description": r[6], "cat_num": r[7]})
    return res


def get_filtered_products(params):
    cursor = conn.cursor()
    cursor.execute("""select * from product where cat_num = %(category)s and 
        price >= %(min_price)s and price <= %(max_price)s and warranty_period >= %(min_warranty)s and 
        warranty_period <= %(max_warranty)s and installment_plan = %(installment)s order by price desc""", params)
    records = cursor.fetchall()
    res = []
    for r in records:
        res.append({"id": r[0], "name": r[1], "price": r[2], "installment_plan": r[3],
                    "warranty_period": r[4], "img_url": r[5], "description": r[6], "cat_num": r[7]})
    return res


def get_low_priced_products(params):
    cursor = conn.cursor()
    cursor.execute("""select * from product where cat_num = %(category)s order by price asc limit %(n)s;""", params)
    records = cursor.fetchall()
    res = []
    for r in records:
        res.append({"id": r[0], "name": r[1], "price": r[2], "installment_plan": r[3],
                    "warranty_period": r[4], "img_url": r[5], "description": r[6], "cat_num": r[7]})
    return res


def get_high_priced_products(params):
    cursor = conn.cursor()
    cursor.execute("""select * from product where cat_num = %(category)s order by price desc limit %(n)s;""", params)
    records = cursor.fetchall()
    res = []
    for r in records:
        res.append({"id": r[0], "name": r[1], "price": r[2], "installment_plan": r[3],
                    "warranty_period": r[4], "img_url": r[5], "description": r[6], "cat_num": r[7]})
    return res


def get_filter_values(cat_num):
    cursor = conn.cursor()
    cursor.execute("""select min(price), max(price), min(warranty_period), max(warranty_period) 
        from product where cat_num = %(cat_num)s""", {"cat_num": cat_num})
    records = cursor.fetchall()
    res = {"min_price": 0, "max_price": 0, "min_warranty": 0, "max_warranty": 0}
    if records:
        res = {"min_price": records[0][0], "max_price": records[0][1], "min_warranty": records[0][2],
               "max_warranty": records[0][3]}
    return res


def get_product_info(id):
    cursor = conn.cursor()
    cursor.execute(
        """select * from product where prod_id = %(id)s""",
        {"id": id})
    records = cursor.fetchall()
    res = {"id": records[0][0], "name": records[0][1], "price": records[0][2], "installment_plan": records[0][3],
           "warranty_period": records[0][4], "img_url": records[0][5], "description": records[0][6],
           "cat_num": records[0][7]}
    return res


def add_or_update_product(data):
    cursor = conn.cursor()
    if data["id"] == "-1":
        cursor.execute("""select max(prod_id) from product""")
        prod_id = cursor.fetchall()[0][0] + 1
        data["id"] = prod_id
        cursor.execute("""insert into product (prod_id, prod_name, price, installment_plan, warranty_period, cat_num, 
        img_url, description) values (%(id)s, %(name)s, %(price)s, %(plan)s, %(period)s, %(num)s, %(image)s, %(desc)s)""",
                      data)
    else:
        cursor.execute("""update product set prod_name = %(name)s, price = %(price)s, 
            installment_plan = %(plan)s, warranty_period = %(period)s, img_url = %(image)s, description = %(desc)s, 
            cat_num = %(num)s where prod_id = %(id)s""", data)
    conn.commit()


def remove_product(id):
    cursor = conn.cursor()
    cursor.execute("""delete from product where prod_id = %(prod_id)s""", {"prod_id": id})
    conn.commit()


def get_shop_info():
    cursor = conn.cursor()
    cursor.execute("""select * from shop""")
    records = cursor.fetchall()
    res = {"name": "None", "phone": "None", "address": "None"}
    if records:
        res = {"name": records[0][0], "phone": records[0][1], "address": records[0][3]}
    return res


def update_shop_info(data):
    cursor = conn.cursor()
    cursor.execute("""update shop set name = %(name)s, phone = %(phone)s, address = %(address)s""",
                   {"name": data["name"],
                    "phone": data["phone"],
                    "address": data["address"]
                    })
    conn.commit()
