import json
from flask import session


def test_login(client):
    response = client.get("/login")
    assert response.status_code == 200
    client.post("/login", data={"username": "admin", "password": "admin"})
    with client:
        client.get("/")
        assert "user" in session


def test_logout(client):
    client.post("/login", data={"username": "admin", "password": "admin"})
    with client:
        client.get("/")
        assert "user" in session
    client.get("/logout")
    with client:
        client.get("/")
        assert "user" not in session


def test_admin(client):
    client.post("/login", data={"username": "admin", "password": "admin"})
    response = client.get("/admin")
    assert response.status_code == 200


def test_category_add(client):
    client.post("/login", data={"username": "admin", "password": "admin"})
    response = client.post("/category?id=-1&parent=1&name=test")
    assert response.status_code == 200
    response = client.get("/category_path?category=1.2")
    data = json.loads(response.data.decode('utf8').replace("'", '"'))
    assert data["path"] == "computers/test"


def test_category_update(client):
    client.post("/login", data={"username": "admin", "password": "admin"})
    response = client.post("/category?id=1.1&parent=1&name=test_new")
    assert response.status_code == 200
    response = client.get("/category_path?category=1.1")
    data = json.loads(response.data.decode('utf8').replace("'", '"'))
    assert data["path"] == "computers/test_new"


def test_category_delete(client):
    client.post("/login", data={"username": "admin", "password": "admin"})
    response = client.delete("/category?id=1.1")
    assert response.status_code == 200


def test_product_add(client):
    client.post("/login", data={"username": "admin", "password": "admin"})
    response = client.post("/product?id=-1&name=test&price=5000&plan=true&period=1&num=1.1&image=test&desc=test")
    assert response.status_code == 200
    response = client.get("/products?category=1.1")
    data = json.loads(response.data.decode('utf8').replace("'", '"'))
    assert data["products"][3]["name"] == "test"


def test_product_update(client):
    client.post("/login", data={"username": "admin", "password": "admin"})
    response = client.post("/product?id=1&name=test&price=5000&plan=true&period=1&num=1.1&image=test&desc=test")
    assert response.status_code == 200
    response = client.get("/products?category=1.1")
    data = json.loads(response.data.decode('utf8').replace("'", '"'))
    assert data["products"][2]["name"] == "test"


def test_product_delete(client):
    client.post("/login", data={"username": "admin", "password": "admin"})
    response = client.delete("/product?id=1")
    assert response.status_code == 200
    response = client.get("/products?category=1.1")
    data = json.loads(response.data.decode('utf8').replace("'", '"'))
    assert len(data["products"]) == 2


def test_shop(client):
    client.post("/login", data={"username": "admin", "password": "admin"})
    response = client.post("/shop?name=test&phone=88888888888&address=test")
    assert response.status_code == 200
