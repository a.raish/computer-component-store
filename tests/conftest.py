import pytest
from shop import app as my_app
from shop import conn


@pytest.fixture
def app():
    cursor = conn.cursor()
    cursor.execute(open("tests/schema_test.sql", "r").read())
    conn.commit()
    return my_app


@pytest.fixture
def client(app):
    return app.test_client()
