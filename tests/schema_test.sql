drop schema public cascade;
create schema public;

create table category(
cat_num varchar(256) primary key,
cat_name varchar(256) not null
);

create table product(
prod_id integer primary key,
prod_name varchar(256),
price integer not null,
installment_plan boolean not null,
warranty_period integer not null,
img_url varchar(256) not null,
description varchar(256) not null,
cat_num varchar(256) not null
);

create table shop(
name varchar(256) primary key,
phone varchar(15) unique,
description varchar(1024) not null,
address varchar(1024) not null);

INSERT INTO shop(name, phone, description, address) VALUES
('shop', '89537925950', 'shopDesc', 'shopAddress');

INSERT INTO category(cat_num, cat_name) VALUES
('1', 'computers'),
('1.1', 'system_block');

INSERT INTO product(prod_id, prod_name, price, installment_plan, warranty_period, img_url, description, cat_num) VALUES
(1, 'sblock_1', 10000, true, 3, 'http://crimeastokcomputer.ru/wp-content/uploads/2019/02/IMG_3286.jpg', 'sblock_1', '1.1'),
(2, 'sblock_2', 20000, false, 4, 'https://ноутбуки.бел/image/cache/catalog/desktops/Gamemax/igrovoi-pk-magnum-550-17-1200x800.jpg', 'sblock_2', '1.1'),
(3, 'sblock_3', 30000, true, 5, 'http://crimeastokcomputer.ru/wp-content/uploads/2019/02/IMG_3286.jpg', 'not sblock_1', '1.1');

create or replace function add_category(N integer, cat_name varchar(256),
parent_cat varchar(256))
RETURNS BOOLEAN AS $$
DECLARE
num_to_add integer := -1;
num_var varchar(256);
BEGIN
select max(coalesce(split_part(cat_num, '.', N), '0'))
into num_var from category;
select CAST(num_var AS integer) into num_to_add;
num_to_add := num_to_add + 1;
IF parent_cat is not null THEN
insert into category
values (parent_cat || '.' || CAST(num_to_add AS varchar(256)), cat_name);
ELSE
insert into category
values (CAST(num_to_add AS varchar(256)), cat_name);
END IF;
return true;

EXCEPTION
WHEN unique_violation THEN
RAISE notice 'Illegal id: %', sqlerrm;
WHEN others THEN
RAISE notice 'SQLSTATE: %', sqlstate;
END
$$ LANGUAGE 'plpgsql';