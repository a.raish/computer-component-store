import json


def test_main(client):
    response = client.get("/")
    assert response.status_code == 200


def test_products(client):
    response = client.get("/products")
    data = json.loads(response.data.decode('utf8').replace("'", '"'))
    assert len(data["products"]) == 0
    response = client.get("/products?category=1.1")
    data = json.loads(response.data.decode('utf8').replace("'", '"'))
    assert len(data["products"]) == 3


def test_filter_values(client):
    response = client.get("/filter_values?category=1.1")
    data = json.loads(response.data.decode('utf8').replace("'", '"'))
    assert data["values"]["min_price"] == 10000 and data["values"]["max_price"] == 30000 and \
           data["values"]["min_warranty"] == 3 and data["values"]["max_warranty"] == 5


def test_filtered_products(client):
    response = client.get("/filtered_products?category=1.1&min_price=15000&max_price=30000&min_warranty=1"
                          "&max_warranty=5&installment=true")
    data = json.loads(response.data.decode('utf8').replace("'", '"'))
    assert len(data["products"]) == 1 and data["products"][0]["id"] == 3


def test_low_priced_products(client):
    response = client.get("/low_priced_products?category=1.1")
    data = json.loads(response.data.decode('utf8').replace("'", '"'))
    assert len(data["products"]) == 3 and data["products"][0]["id"] == 1 and \
           data["products"][1]["id"] == 2 and data["products"][2]["id"] == 3


def test_high_priced_products(client):
    response = client.get("/high_priced_products?category=1.1")
    data = json.loads(response.data.decode('utf8').replace("'", '"'))
    assert len(data["products"]) == 3 and data["products"][0]["id"] == 3 and \
           data["products"][1]["id"] == 2 and data["products"][2]["id"] == 1


def test_category_path(client):
    response = client.get("/category_path?category=1.1")
    data = json.loads(response.data.decode('utf8').replace("'", '"'))
    assert data["path"] == "computers/system_block"


def test_category_info(client):
    response = client.get("/category_info?category=1.1")
    data = json.loads(response.data.decode('utf8').replace("'", '"'))
    assert data["info"]["avg_price"] == 20000.0 and data["info"]["min_price"] == 10000.0 and \
           data["info"]["max_price"] == 30000.0 and data["info"]["count"] == 3


def test_product_info(client):
    response = client.get("/product_info?id=1")
    data = json.loads(response.data.decode('utf8').replace("'", '"'))
    assert data["products"]["id"] == 1 and data["products"]["name"] == "sblock_1" and \
           data["products"]["price"] == 10000 and data["products"]["installment_plan"] == True and \
           data["products"]["warranty_period"] == 3 and data["products"]["img_url"] == \
                "http://crimeastokcomputer.ru/wp-content/uploads/2019/02/IMG_3286.jpg" and \
           data["products"]["description"] == "sblock_1" and data["products"]["cat_num"] == "1.1"
