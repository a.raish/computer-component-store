#!/bin/bash

docker-compose up -d
ID=`docker ps -l -q`
APP_PATH=`docker exec $ID pwd`
docker exec $ID coverage run -m pytest
docker exec $ID coverage xml
docker cp $ID:$APP_PATH/coverage.xml coverage.xml
docker stop $ID
sonar-scanner -D"sonar.projectKey=CCS" -D"sonar.sources=." -D"sonar.host.url=http://84.237.50.237:9000" -D"sonar.login=42ff4249e49dcce2b714ad42139ee9f7d5214403"