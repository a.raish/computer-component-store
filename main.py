from shop import app, conn


if __name__ == '__main__':
    # Init database
    cursor = conn.cursor()
    cursor.execute(open("shop/schema.sql", "r").read())
    conn.commit()
    # Run app
    app.run(debug=True, host="0.0.0.0")
