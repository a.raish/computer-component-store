#### Computer component store

Requirements: pytest, coverage

Запуск тестов: python -m pytest tests

Запуск расчета покрытия: coverage run -m pytest

Запуск консольного отчета по покрытию: coverage report

Запуск сохранения отчета по покрытию в виде html: coverage html

Запуск сохранения отчета по покрытию в виде xml: coverage xml
